#!/bin/bash

verb="$1"
section_id="$2"
section_title="${3:-$section_id}"

if [ "$verb" = "start" ]; then
	echo -e "\e[0Ksection_start:$(date +%s):${section_id}\r\e[0K${section_title}"
elif [ "$verb" = "collapsed" ]; then
	echo -e "\e[0Ksection_start:$(date +%s):${section_id}[collapsed=true]\r\e[0K${section_title}"
elif [ "$verb" = "end" ]; then
	echo -e "\e[0Ksection_end:$(date +%s):${section_id}\r\e[0K"
else
	echo "ERROR: section.sh: invalid argument: $verb, expected start or end"
fi
