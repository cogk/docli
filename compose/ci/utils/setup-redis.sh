#!/bin/bash

if [ "$1" = "v1" ]; then
	set -xeu -o pipefail
	bench config set-common-config \
		-c redis_queue redis://redis_queue:6379 \
		-c redis_cache redis://redis_cache:6379 \
		-c redis_socketio redis://redis_socketio:6379
else
	echo "ERROR: setup-redis.sh: invalid argument: $1"
	exit 1
fi
