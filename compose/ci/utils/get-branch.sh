#!/bin/bash

get-branch() {
	if [ -n "${OVERRIDE_BUILD_BRANCH}" ]; then
		echo "${OVERRIDE_BUILD_BRANCH}"
	elif [[ "${CI_COMMIT_TAG}" =~ ^v[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
		echo "${CI_COMMIT_TAG}" | cut -d'.' -f1 # v4
	elif [[ "${CI_COMMIT_REF_NAME}" =~ ^v[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
		echo "${CI_COMMIT_REF_NAME}" | cut -d'.' -f1 # v4
	elif [[ "${CI_COMMIT_REF_NAME}" =~ ^v[0-9]+(-[a-z]+)?$ ]]; then
		echo "${CI_COMMIT_REF_NAME}" # v4-fix, v4-dev, v4
	elif [ -n "${CI_MERGE_REQUEST_TARGET_BRANCH_PROTECTED}" ] && [ "${CI_MERGE_REQUEST_TARGET_BRANCH_PROTECTED}" = "true" ]; then
		echo "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
	elif [ -n "${CI_COMMIT_REF_PROTECTED}" ] && [ "${CI_COMMIT_REF_PROTECTED}" = "true" ]; then
		echo "${CI_COMMIT_REF_NAME}"
	else
		echo "develop"
	fi
}

get-branch
