#!/bin/bash

if [ "$1" = "v1" ]; then
	set -xeu -o pipefail
	sed -i 's/^\(watch\|schedule\|socketio\|redis_socketio\):/# \1:/g' Procfile
else
	echo "ERROR: patch-procfile.sh: invalid argument: $1"
	exit 1
fi
