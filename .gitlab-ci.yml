include:
  - local: "/.gitlab-ci.base.yml"

variables:
  IMAGE_TAG: "ci-$CI_COMMIT_REF_SLUG"
  IMAGE_NAME: "$CI_REGISTRY_IMAGE:$IMAGE_TAG"

stages:
  - Test
  - Build

test:
  stage: Test
  image: "python:3.10-buster"

  services:
    - name: mariadb:10.6
      command: ['mysqld', '--character-set-server=utf8mb4', '--collation-server=utf8mb4_unicode_ci', '--character-set-client-handshake=FALSE', '--innodb_read_only_compressed=OFF']
      alias: db
    - name: redis
      alias: redis_queue
    - name: redis
      alias: redis_cache
    - name: redis
      alias: redis_socketio

  variables:
    MARIADB_DATABASE: "test_dodock"
    MARIADB_ROOT_PASSWORD: "test_dodock"

  before_script:
    - apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-suggests --no-install-recommends mariadb-client xfonts-75dpi xfonts-base redis-server
    - export LANG=C.UTF-8
    - export LC_ALL=C.UTF-8
    - export LANGUAGE=en_US.UTF-8
    - wget -q https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.buster_amd64.deb
    - dpkg -i wkhtmltox_0.12.6-1.buster_amd64.deb && rm wkhtmltox_0.12.6-1.buster_amd64.deb
    - rm -rf /var/lib/apt/lists/*
    - rm -rf /etc/apt/sources.list.d/*
    - mkdir -p /etc/apt/keyrings
    - curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
    - echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_18.x nodistro main" > /etc/apt/sources.list.d/nodesource.list
    - apt-get -q update
    - apt-get install -y nodejs
    - npm install --global yarn
    - export PATH=$PATH:/home/dokotest/.local/bin
    - adduser --disabled-password --gecos "" dokotest
    - usermod -aG sudo dokotest
    - echo 'dokotest ALL=(ALL)  NOPASSWD:ALL' >> /etc/sudoers

  script:
    - su -c "pip install ." dokotest
    - bench-set-base-config() { su -c "cd $BENCH && bench config set-common-config -c redis_queue redis://redis_queue:6379 -c redis_cache redis://redis_cache:6379 -c redis_socketio redis://redis_socketio:6379 -c root_password $MARIADB_ROOT_PASSWORD -c db_host db -c db_port 3306 -c db_type mariadb -c admin_password admin" dokotest; }

    # https://doc.dokos.io/dodock/installation/manuelle
    - export BENCH="/home/dokotest/bench1"
    - su -c "bench init $BENCH --no-backups --version develop --install-app dokos --install-app hrms" dokotest
    - bench-set-base-config
    - su -c "cd $BENCH && (bench start > bench_start.log) &" dokotest
    - su -c "cd $BENCH && bench new-site test.local --set-default --install-app dokos --install-app hrms" dokotest
    - su -c "cd $BENCH && bench build --app frappe" dokotest
    - su -c "cd $BENCH && bench build --app payments" dokotest
    - su -c "cd $BENCH && bench build --app hrms" dokotest
    - su -c "cd $BENCH && bench build --app erpnext" dokotest
    - tail -n 10 $BENCH/bench_start.log

    - export BENCH="/home/dokotest/bench2"
    - su -c "bench init $BENCH --no-backups --version v3.x.x-hotfix --install-app dokos" dokotest
    - bench-set-base-config
    - su -c "cd $BENCH && (bench start > bench_start.log) &" dokotest
    - su -c "cd $BENCH && bench new-site test.local --set-default --install-app dokos" dokotest
    - ls $BENCH/sites/assets/frappe/dist/js/desk.bundle.*.js
    - ls $BENCH/sites/assets/frappe/dist/css/desk.bundle.*.css
    - ls $BENCH/sites/assets/erpnext/dist/css/erpnext.bundle.*.css
    - ls $BENCH/sites/assets/erpnext/dist/js/erpnext.bundle.*.js
    - tail -n 10 $BENCH/bench_start.log

docker_build:
  stage: Build
  image: docker:stable
  rules:
    - if: $CI_COMMIT_REF_PROTECTED == "true"
  services:
    - docker:dind
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $IMAGE_NAME || true
    - docker build --cache-from $IMAGE_NAME -t $IMAGE_NAME -f compose/ci/Dockerfile compose/ci
    - docker push $IMAGE_NAME

docker_test:
  stage: Build
  image: $IMAGE_NAME
  rules:
    - if: $CI_COMMIT_REF_PROTECTED == "true"
  services:
    - name: mariadb:10.6
      command: ['mysqld', '--character-set-server=utf8mb4', '--collation-server=utf8mb4_unicode_ci', '--character-set-client-handshake=FALSE', '--innodb_read_only_compressed=OFF']
      alias: db
    - name: redis
      alias: redis_queue
    - name: redis
      alias: redis_cache
    - name: redis
      alias: redis_socketio
  variables:
    BASE_BRANCH: "develop"
    MARIADB_DATABASE: "test_dodock"
    MARIADB_ROOT_PASSWORD: "test_dodock"
  before_script:
    - cd "$BENCH_DIR"
    - /ci-utils/section.sh collapsed bench_init "Initializing bench at $BENCH_DIR"
    - bench init . --ignore-exist --no-backups --skip-redis-config-generation --skip-assets --frappe-branch $BASE_BRANCH
    - env/bin/python -m pip install --quiet hypothesis==6.68.2 unittest-xml-reporting
    - /ci-utils/setup-redis.sh v1
    - /ci-utils/patch-procfile.sh v1
    - cp -r apps/frappe/test_sites/test_site sites/
    - /ci-utils/section.sh end bench_init
  script:
    - bench get-app https://gitlab.com/dokos/payments.git --branch develop

    # A running server might be needed to install some apps / sites
    - (bench start > bench_start.log) &
    - pid=$!

    - bench --site test_site reinstall --yes --mariadb-root-password "$MARIADB_ROOT_PASSWORD" --admin-password "admin"
    - bench --site test_site install-app payments
    - bench build --app payments

    - bench use test_site
    - kill $pid
    - (bench start >> bench_start.log) &
    - pid=$!

    - curl http://localhost:8000/api/method/ping || true
    - cat bench_start.log
